import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: "token",
    chat_id: "chat-id",
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
